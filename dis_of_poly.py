from sympy import *
from sympy.abc import a,b,c,d,e,f,g,h,x
init_printing()
def list_move(cof_lst,i): # cof_lst为系数列表，i为循环移动的位数
    moved_lst = [cof_lst[(j-i)%len(cof_lst)] for j in range(len(cof_lst))]
    return moved_lst

def make_matrix(cof_a,cof_b,n): # a,b为列表，n为多项式次数
    matrix_lst = []
    for i in range(n-1):
        matrix_lst.append(list_move(cof_a,i))
    for j in range(n):
        matrix_lst.append(list_move(cof_b,j))
    return matrix_lst

if __name__ == '__main__':
    dict_a = {}
    dict_b = {}
    dict_a['2'] = [a,b,c]
    dict_b['2'] = [2*a,b,0]
    dict_a['3'] = [a,b,c,d,0]
    dict_b['3'] = [3*a,2*b,c,0,0]
    dict_a['4'] = [a,b,c,d,e,0,0]
    dict_b['4'] = [4*a,3*b,2*c,d,0,0,0]
    dict_a['5'] = [a,b,c,d,e,f,0,0,0]
    dict_b['5'] = [5*a,4*b,3*c,2*d,e,0,0,0,0]
    dict_a['6'] = [a,b,c,d,e,f,g,0,0,0,0]
    dict_b['6'] = [6*a,5*b,4*c,3*d,2*e,f,0,0,0,0,0]
    dict_a['7'] = [a,b,c,d,e,f,g,h,0,0,0,0,0]
    dict_b['7'] = [7*a,6*b,5*c,4*d,3*e,2*f,g,0,0,0,0,0,0]
