from dis_of_poly import *
from sympy import *
from sympy.abc import a,b,c,d,e,f,g,h,x
import math
import datetime

n = eval(input("Enter degree(2-6):"))
d1 = datetime.datetime.now()
#计算判别式
cof_a = dict_a[str(n)]
cof_b = dict_b[str(n)]
dis_matrix = Matrix(make_matrix(cof_a, cof_b, n))
s = n*(n-1)/2
if s%2 == 0: 
    result = simplify(1/a*dis_matrix.det())
else:
    result = simplify(-1/a*dis_matrix.det())
#输出    
d2 = datetime.datetime.now()    
print("Run successfully!一共用时：",d2-d1)
